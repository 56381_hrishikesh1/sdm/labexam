CREATE TABLE Movie(movie_id INT PRIMARY KEY AUTO_INCREMENT, movie_title VARCHAR(50), movie_release_date VARCHAR(10),movie_time VARCHAR(10),director_name VARCHAR(50));

INSERT INTO Movie VALUES (DEFAULT, 'Avengers', '12-12-2019','18:30','Jeet');
INSERT INTO Movie VALUES (DEFAULT, 'Avengers 2', '09-02-2020','09:30','DD');
INSERT INTO Movie VALUES (DEFAULT, 'Dr. Strange', '23-04-2018','15:30','Hrishi');