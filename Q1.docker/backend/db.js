const mysql = require('mysql2')

const openConnection = () =>{
    const connection = mysql.createConnection({
        host: 'database',
        user: 'root',
        password: 'root',
        database: 'sdmlabexam',
        port:3306
    })
    connection.connect()
    return connection
}

module.exports = {
    openConnection,
}