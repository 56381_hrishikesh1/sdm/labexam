const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/getMovies',(request,response)=>{
    const statement = `SELECT * FROM Movie`
    const connection = db.openConnection()
    connection.query(statement, (error,data)=>{
        connection.end()
        response.send(utils.createResult(error,data))
    })
})

router.post('/addMovie',(request,response)=>{
    const { movie_title, movie_release_date,movie_time,director_name } = request.body
    const statement = `
    INSERT INTO Movie 
    (movie_title, movie_release_date,movie_time,director_name)
    VALUES
    ('${movie_title}', '${movie_release_date}','${movie_time}','${director_name}')
    `
    const connection = db.openConnection()
    connection.query(statement, (error,data)=>{
        connection.end()
        response.send(utils.createResult(error,data))
    })
})

router.put('/updateMovie/:movie_id',(request,response)=>{
    const { movie_id } = request.params
    const {movie_release_date,movie_time} = request.body
    const statement = `
    UPDATE Movie
    SET
    movie_release_date = '${movie_release_date}',
    movie_time = '${movie_time}'
    WHERE movie_id = ${movie_id}
    `
    const connection = db.openConnection()
    connection.query(statement, (error,data)=>{
        connection.end()
        response.send(utils.createResult(error,data))
    })
})

router.delete('/deleteMovie/:movie_id',(request,response)=>{
    const { movie_id } = request.params
    const statement = `DELETE FROM Movie WHERE movie_id = ${movie_id} `
    const connection = db.openConnection()
    connection.query(statement, (error,data)=>{
        connection.end()
        response.send(utils.createResult(error,data))
    })
})

module.exports = router
